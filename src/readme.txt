stylus 使用method
首先，安装stylus,执行npm i stylus -g
编译stylus为css文件，执行stylus --compress css/(css文件目录下的.styl文件)
自动编译stylus为css文件，执行stylus -w .styl(要编译的.styl文件名称) -o .css(输出的.css文件名称)

src
+index.html
+css
++index.styl
++test.styl

1.单文件编译
cd css目录下，执行stylus --compress <index.styl> index.css，手动编译index.styl为index.css
2.多文件编译
cd src目录下，执行stylus --compress css/ 手动编译index.styl和test.styl文件为.css文件名称
3.自动编译
cd css目录下，执行stylus -w(自动监听) .styl(监听.styl文件名称) -o(将编译后的css文件输出到指定文件中) .css(输出.css文件名称)
4.批量自动编译
cd src目录下，执行stylus -w css/ -o css/
