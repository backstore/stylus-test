# stylus-test

####  **项目介绍** 


#### 软件架构
软件架构说明


#### 安装教程
stylus 使用method
首先，安装stylus,执行npm i stylus -g
编译stylus为css文件，执行stylus --compress css/(css文件目录下的.styl文件)
自动编译stylus为css文件，执行stylus -w .styl(要编译的.styl文件名称) -o .css(输出的.css文件名称)

#使用说明


####  src
####  +index.html
####  +css
####  ++index.styl
####  ++test.styl



- **单文件编译** 

####  cd css目录下，执行stylus --compress <index.styl> index.css，手动编译index.styl为index.css
- **多文件编译** 

####  cd src目录下，执行stylus --compress css/ 手动编译index.styl和test.styl文件为.css文件名称
- **自动编译** 

####  cd css目录下，执行stylus -w(自动监听) .styl(监听.styl文件名称) -o(将编译后的css文件输出到指定文件中) .css(输出.css文件名称)
- **批量自动编译** 

####  cd src目录下，执行stylus -w css/ -o css/

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)